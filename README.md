
# Prerequisites
* docker
* docker-compose

# How to
1. Rename ".env.example" to .env
2. Enter required values in .env
3. Run <code>./vpn.sh build_and_start</code>
4. Optional: Watch logs with <code>./vpn.sh logs</code>

# VPN script command list
* <code>./vpn.sh build</code>
* <code>./vpn.sh start</code>
* <code>./vpn.sh stop</code>
* <code>./vpn.sh build_and_start</code>
* <code>./vpn.sh logs</code>