#!/bin/bash

run() { 
  echo "\$ $@" ; 
  "$@" ; 
}

if [[ $1 == "build" ]]; then
  run docker-compose build openvpn-applications
elif [[ $1 == "start" ]]; then
  run docker-compose up -d
elif [[ $1 == "build_and_start" ]]; then
  run docker-compose build openvpn-applications
  run docker-compose up -d
elif [[ $1 == "stop" ]]; then
  run docker-compose down
elif [[ $1 == "logs" ]]; then
  OPTIONS=$2
  run docker-compose logs $OPTIONS openvpn-applications
else 
  echo "Unrecognized command"
fi